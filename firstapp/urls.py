from django.urls import path
from .views import UserViewSet, CategoryViewSet, PostViewSet, ApiRoot
from rest_framework.urlpatterns import format_suffix_patterns

PostList = PostViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
PostDetail = PostViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

CategoryList = CategoryViewSet.as_view({
    'get': 'list',

})
CategoryDetail = CategoryViewSet.as_view({
    'get': 'retrieve',

})

UserList = UserViewSet.as_view({
    'get': 'list',

})
UserDetail = UserViewSet.as_view({
    'get': 'retrieve',

})

urlpatterns = [
    path('', ApiRoot.as_view(), name='root'),
    path('categories/', CategoryList, name='categories'),
    path('categories/<int:pk>/', CategoryDetail, name='single_category'),
    path('users/', UserList, name='users'),
    path('users/<int:pk>/', UserDetail, name='single-user'),
    path('posts/', PostList, name='posts'),
    path('posts/<int:pk>/', PostDetail,
         name='single-post'),  # pk de jga id v likh SAKDE
]
urlpatterns = format_suffix_patterns(urlpatterns)
